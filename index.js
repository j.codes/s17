/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserInfo(){
	let fullName = prompt('What is your name?');
	let age = prompt('How old are you?');
	let address = prompt('Where do you live?');

	alert('Thank you for your input!')

	console.log('Hello, ' + fullName);
	console.log('You are ' + age + ' years old.');
	console.log('You live in ' + address);
	};

	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFavoriteBands(){
		let bandList = ["Ben and Ben", "Rivermaya", "Itchyworms", "Moonstar88", "Orange and Lemons"]

		console.log('1. ' + bandList[0]);
		console.log('2. ' + bandList[1]);
		console.log('3. ' + bandList[2]);
		console.log('4. ' + bandList[3]);
		console.log('5. ' + bandList[4]);
	}


	displayFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayFavoriteMovies(){

		let movieList = ["Three Minutes - A Lengthening", "The Discreet Charm of the Bourgeoisie", "Jaws", "The Conversation", "Mrs. Haris Goes to Paris"]
		let movieRating = ["100%", "98%", "97%", "97%","95%"]

		console.log("1. " + movieList[0] + "\nRotten Tomatoes Rating: " + movieRating[0]);
		console.log("2. " + movieList[1] + "\nRotten Tomatoes Rating: " + movieRating[1]);
		console.log("3. " + movieList[2] + "\nRotten Tomatoes Rating: " + movieRating[2]);
		console.log("4. " + movieList[3] + "\nRotten Tomatoes Rating: " + movieRating[3]);
		console.log("5. " + movieList[4] + "\nRotten Tomatoes Rating: " + movieRating[4]);

		/*console.log("1. Three Minutes - A Lengthening");
		console.log("Rotten Tomatoes Rating: 100%");

		console.log("2. The Discreet Charm of the Bourgeoisie");
		console.log("Rotten Tomatoes Rating: 98%");

		console.log("3. Jaws");
		console.log("Rotten Tomatoes Rating: 97%");

		console.log("4. The Conversation");
		console.log("Rotten Tomatoes Rating: 97%");

		console.log("5. Mrs. Haris Goes to Paris");
		console.log("Rotten Tomatoes Rating: 95%");*/
	}

	displayFavoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};

	printFriends();

	/*console.log(friend1);
	console.log(friend2);*/